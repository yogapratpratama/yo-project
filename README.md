# Springboot Microservices Project
This is an prototype of backend server with Microservice architecture made using Springboot. This project is not finished and can still be updated to be even better.


## Already implements
- Implement Microservices Architectures on Springboot Projects as a backend services
- Implement Discovery services using Eureka Server for automatic registration all services and automatic find ip and hostname
- Implement Gateway services using Springcloud gateway for routing every end point on microservices
